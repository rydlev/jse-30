package ru.t1.rydlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.DataXmlSaveFasterXmlRequest;

public final class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("DATA SAVE XML");
        @NotNull final DataXmlSaveFasterXmlRequest request = new DataXmlSaveFasterXmlRequest(getToken());
        getDomainEndpointClient().saveDataXmlFasterXml(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in xml file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-xml";
    }

}
