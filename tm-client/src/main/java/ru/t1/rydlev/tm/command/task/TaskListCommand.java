package ru.t1.rydlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.dto.request.TaskListRequest;
import ru.t1.rydlev.tm.enumerated.Sort;
import ru.t1.rydlev.tm.model.Task;
import ru.t1.rydlev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(getToken());
        request.setSort(sort);
        @Nullable final List<Task> tasks = getTaskEndpoint().listTask(request).getTasks();
        if (tasks == null) return;
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task list.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

}
