package ru.t1.rydlev.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.model.User;

@NoArgsConstructor
public final class UserViewProfileResponse extends AbstractUserResponse {

    public UserViewProfileResponse(@Nullable final User user) {
        super(user);
    }

}
