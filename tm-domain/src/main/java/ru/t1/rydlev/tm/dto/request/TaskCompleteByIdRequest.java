package ru.t1.rydlev.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCompleteByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskCompleteByIdRequest(@Nullable final String token) {
        super(token);
    }

}
