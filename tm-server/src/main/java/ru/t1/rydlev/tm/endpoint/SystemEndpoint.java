package ru.t1.rydlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.api.endpoint.ISystemEndpoint;
import ru.t1.rydlev.tm.api.service.IPropertyService;
import ru.t1.rydlev.tm.api.service.IServiceLocator;
import ru.t1.rydlev.tm.dto.request.ServerAboutRequest;
import ru.t1.rydlev.tm.dto.request.ServerVersionRequest;
import ru.t1.rydlev.tm.dto.response.ServerAboutResponse;
import ru.t1.rydlev.tm.dto.response.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.rydlev.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint implements ISystemEndpoint {

    @NotNull
    private final IServiceLocator serviceLocator;

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRequest request
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRequest request
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
